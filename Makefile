#
#	patch a binary .axf file with a new set of kernel parameters
#

CC ?= gcc
CFLAGS ?= -g

OBJ = paxf.o


all: paxf

paxf: paxf.o
	$(CC) $(CFLAGS) -o paxf $(OBJ)

clean:
	rm -f paxf $(OBJ)

.c.o:
	$(CC) $(CFLAGS) -c $<

